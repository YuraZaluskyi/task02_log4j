package com.yurazaluskyi.brand;

import org.apache.logging.log4j.*;

public class Computer {
    private String name;
    private static Logger logger = LogManager.getLogger(Computer.class);

    public Computer(String name) {
        this.name = name;
    }

    public void generateMessage() {
        logger.trace("This is a trace message from {}", this.name);
        logger.debug("This is a debug message from {}", this.name);
        logger.info("This is an info message from {}", this.name);
        logger.warn("This is a warn message from {}", this.name);
        logger.error("This is an error message from {}", this.name);
        logger.fatal("This is an fatal message from {}", this.name);
    }
}
