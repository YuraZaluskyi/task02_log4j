package com.yurazaluskyi;

import com.yurazaluskyi.brand.Computer;
import com.yurazaluskyi.brand.MobilePhone;
import com.yurazaluskyi.tool.Axe;
import com.yurazaluskyi.tool.Hammer;
import com.yurazaluskyi.transport.Bike;
import com.yurazaluskyi.transport.Bus;
import org.apache.logging.log4j.*;

import java.io.IOException;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);


    public static void main(String[] args) {

        //send log to mail
        MobilePhone mobilePhone = new MobilePhone("ASUS ZENFONE");
        mobilePhone.generateMessage();

        //send log to sms
        Bus bus = new Bus("the bus is late");
        bus.generateMessage();
        
        logger1.error("Exception", new IOException("Hello exception!!!"));

        logger1.trace("Trace message");
        logger1.debug("Debug message");
        logger1.info("Info message");
        logger1.warn("Warn message");
        logger1.error("Error message");
        logger1.fatal("Fatal message");

        Computer computer = new Computer("DELL");
        Bike bike = new Bike("Super Bike");
        Axe axe = new Axe("Battle axe");
        Hammer hammer = new Hammer("Battle hammer");

        computer.generateMessage();
        bike.generateMessage();
        axe.generateMessage();
        hammer.generateMessage();


    }
}
