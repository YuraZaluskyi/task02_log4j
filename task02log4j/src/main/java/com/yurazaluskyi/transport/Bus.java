package com.yurazaluskyi.transport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bus {
    private String name;
    private static Logger logger = LogManager.getLogger(Bus.class);

    public Bus(String name) {
        this.name = name;
    }

    public void generateMessage() {
        logger.trace("This is a trace message", this.name);
        logger.debug("This is a debug message", this.name);
        logger.info("This is an info message", this.name);
        logger.warn("This is a warn message", this.name);
        logger.error("This is an error message", this.name);
        logger.fatal("This is a fatal message", this.name);
    }
}
